// Model: Robots
//  class = light, medium, heavy
//  hardpoints = standard, medium, heavy

var robotImagePath = "images/robots/";
var weaponImagePath = "images/weapons/";

// Model: Menu
var menu = [
    {
        "id": 0,
        "label": "Start",
        "url": "start"
    },
    {
        "id": 1,
        "label": "Robots",
        "url": "listrobots"
    },
    {
        "id": 2,
        "label": "Weapons",
        "url": "listweapons"
    },
    {
        "id": 3,
        "label": "Compare Weapons",
        "url": "compareweapons"
    },
    {
        "id": 4,
        "label": "Maps",
        "url": "maps"
    },
    {
        "id": 5,
        "label": "Releases",
        "url": "releases"
    },
    {
        "id": 6,
        "label": "Links",
        "url": "links"
    },
    {
        "id": 7,
        "label": "About",
        "url": "about"
    }
];

var menuShort = [0,1,2,3,4];

// Model: Robots
var robots = [
    {
        "id": "0",
        "name": "Destrier",
        "power": 440,
        "weight": 45,
        "durability": 29000,
        "class": "light",
        "level": 1,
        "playerlevel": 1,
        "cost": 50000,
        "currency": "silver",
        "speed": 60,
        "info": "Starter model, quick when upgraded",
        "image": robotImagePath + "destrier.jpg",
        "maxlevel": 6,
        "hardpoints": [
            {"name": "HP1", "slot": "standard"},
            {"name": "HP2", "slot": "standard"}
        ]
    },
    {
        "id": "1",
        "name": "Cossack",
        "power": 440,
        "weight": 45,
        "durability": 26000,
        "class": "light",
        "level": 1,
        "playerlevel": 5,
        "cost": 50000,
        "currency": "silver",
        "speed": 70,
        "image": robotImagePath + "cossack.jpg",
        "maxlevel": 6,
        "hardpoints": [
            {"name": "HP1", "slot": "medium"}
        ]
    },
    {
        "id": "2",
        "name": "Schütze",
        "power": 440,
        "weight": 60,
        "durability": 26000,
        "class": "light",
        "level": 1,
        "playerlevel": 2,
        "cost": 50000,
        "currency": "silver",
        "speed": 55,
        "image": robotImagePath + "schutze.jpg",
        "maxlevel": 6,
        "hardpoints": [
            {"name": "HP1", "slot": "heavy"}
        ]
    },
    {
        "id": "3",
        "name": "Boa",
        "power": 400,
        "weight": 100,
        "durability": 42500,
        "class": "medium",
        "level": 1,
        "playerlevel": 5,
        "cost": 500,
        "currency": "gold",
        "speed": 48,
        "image": robotImagePath + "boa.jpg",
        "maxlevel": 5,
        "hardpoints": [
            {"name": "HP1", "slot": "medium"},
            {"name": "HP2", "slot": "heavy"}
        ]
    },
    {
        "id": "4",
        "name": "Vityaz",
        "power": 400,
        "weight": 75,
        "durability": 37500,
        "class": "medium",
        "level": 1,
        "playerlevel": 4,
        "cost": 100000,
        "currency": "silver",
        "speed": 42,
        "image": robotImagePath + "vityaz.jpg",
        "maxlevel": 6,
        "hardpoints": [
            {"name": "HP1", "slot": "standard"},
            {"name": "HP2", "slot": "standard"},
            {"name": "HP3", "slot": "heavy"}
        ]
    },
    {
        "id": "5",
        "name": "GI.Patton",
        "power": 400,
        "weight": 75,
        "durability": 42500,
        "class": "medium",
        "level": 1,
        "playerlevel": 4,
        "cost": 100000,
        "currency": "silver",
        "speed": 42,
        "image": robotImagePath + "gipatton.jpg",
        "maxlevel": 6,
        "hardpoints": [
            {"name": "HP1", "slot": "standard"},
            {"name": "HP2", "slot": "standard"},
            {"name": "HP3", "slot": "standard"},
            {"name": "HP4", "slot": "standard"}
        ]
    },

    {
        "id": "6",
        "name": "Leo",
        "power": 353,
        "weight": 132,
        "durability": 76320,
        "class": "heavy",
        "level": 1,
        "playerlevel": 6,
        "cost": 750000,
        "currency": "silver",
        "speed": 30,
        "image": robotImagePath + "leo.jpg",
        "maxlevel": 4,
        "hardpoints": [
            {"name": "HP1", "slot": "standard"},
            {"name": "HP2", "slot": "standard"},
            {"name": "HP3", "slot": "standard"},
            {"name": "HP4", "slot": "heavy"}
        ]
    },
    {
        "id": "7",
        "name": "Natascha",
        "power": 353,
        "weight": 132,
        "durability": 76320,
        "class": "heavy",
        "level": 1,
        "playerlevel": 6,
        "cost": 750000,
        "currency": "silver",
        "speed": 24,
        "image": robotImagePath + "natasha.jpg",
        "maxlevel": 4,
        "hardpoints": [
            {"name": "HP1", "slot": "standard"},
            {"name": "HP2", "slot": "standard"},
            {"name": "HP3", "slot": "heavy"},
            {"name": "HP4", "slot": "heavy"}
        ]
    }
];

// Model: Weapons
//  slot: standard, medium, heavy
//  ammotype: bullet, rocket, mortar, cannon,
//  aimingtype: direct, homing
//
//  No object at index 12 (for testing)
var weapons = [
    {
        "id": 0,
        "name": "GAU Punisher",
        "image": weaponImagePath + "punisher.png",
        "slot": "standard",
        "ammotype": "bullet",
        "aimingtype": "direct",
        "maxlevel": 12,
        "level": 0,
        "playerlevel": 1,
        "damage": 78,
        "firerate": 20,
        "clipsize": 400,
        "reload": 10,
        "range": 500,
        "weight": 9,
        "power": 5,
        "cost": 500,
        "currency": "silver",
        "info": "Deadly in close combat, use in tight quarters when distance to target is less then 200 meters. Almost unusable at longer distances.",
        "durability": 0
    },
    {
        "id": 1,
        "name": "GAU Punisher T",
        "image": weaponImagePath + "punisher-t.png",
        "slot": "medium",
        "ammotype": "bullet",
        "aimingtype": "direct",
        "maxlevel": 12,
        "level": 0,
        "playerlevel": 1,
        "damage": 240,
        "firerate": 20,
        "clipsize": 400,
        "reload": 10,
        "range": 500,
        "weight": 20,
        "power": 10,
        "cost": 250,
        "currency": "gold",
        "info": "Twin-barrel version of GAU Punisher. Deadly in close combat, use in tight quarters when distance to target is less then 200 meters. Great finisher.",
        "durability": 0
    },
    {
        "id": 4,
        "name": "AT Spiral",
        "image": weaponImagePath + "spiral.png",
        "slot": "standard",
        "ammotype": "rocket",
        "aimingtype": "homing",
        "maxlevel": 12,
        "level": 0,
        "playerlevel": 1,
        "damage": 775,
        "firerate": 5,
        "clipsize": 3,
        "reload": 12,
        "range": 600,
        "weight": 15,
        "power": 25,
        "cost": 500,
        "currency": "silver",
        "info": "Basically never misses the target. Aiming need some time to accuire target, so don't shoot until weapon is fully focused on target. Long reload time.",
        "durability": 0
    },
    {
        "id": 2,
        "name": "AC Malot",
        "image": weaponImagePath + "malot.png",
        "slot": "standard",
        "ammotype": "bullet",
        "aimingtype": "direct",
        "maxlevel": 12,
        "level": 0,
        "playerlevel": 2,
        "damage": 150,
        "firerate": 6.7,
        "clipsize": 100,
        "reload": 10,
        "range": 800,
        "weight": 9,
        "power": 5,
        "cost": 2000,
        "currency": "silver",
        "info": "Perfect weapon for open fight at 500 meters distance. Less power at longer distances, and comes short in damage in close combat compared to short-range weapons.",
        "durability": 0
    },
    {
        "id": 3,
        "name": "AC Malot T",
        "image": weaponImagePath + "malot-t.png",
        "slot": "medium",
        "ammotype": "bullet",
        "aimingtype": "direct",
        "maxlevel": 12,
        "level": 0,
        "playerlevel": 5,
        "damage": 162,
        "firerate": 13.3,
        "clipsize": 200,
        "reload": 10,
        "range": 800,
        "weight": 20,
        "power": 10,
        "cost": 250,
        "currency": "gold",
        "info": "Twin-barrel version of AC Malot. Perfect weapon for open fight at 500 meters distance. Less power at longer distances, and comes short in damage in close combat compared to short-range weapons.",
        "durability": 0
    },

    {
        "id": 13,
        "name": "Ecu",
        "image": weaponImagePath + "ecu.png",
        "slot": "standard",
        "ammotype": "",
        "aimingtype": "",
        "maxlevel": 8,
        "level": 0,
        "playerlevel": 3,
        "damage": 0,
        "firerate": 0,
        "clipsize": 0,
        "reload": 0,
        "range": 0,
        "weight": 10,
        "power": 0,
        "cost": 160000,
        "currency": "silver",
        "durability": 49766
    },
    {
        "id": 5,
        "name": "EE Aphid",
        "image": weaponImagePath + "aphid.png",
        "slot": "standard",
        "ammotype": "rocket",
        "aimingtype": "homing",
        "maxlevel": 8,
        "level": 0,
        "playerlevel": 5,
        "damage": 1068,
        "firerate": 8, //0.1
        "clipsize": 8,
        "reload": 10,
        "range": 350,
        "weight": 20,
        "power": 15,
        "cost": 250,
        "currency": "gold",
        "info": "High damage, high projection that easily flies over low obstacles. Low range (350 meters), weak against fast targets.",
        "durability": 0
    },
    {
        "id": 6,
        "name": "SURA-F Pinata",
        "image": weaponImagePath + "pinata.png",
        "slot": "standard",
        "ammotype": "rocket",
        "aimingtype": "direct",
        "maxlevel": 8,
        "level": 0,
        "playerlevel": 4,
        "damage": 1025,
        "firerate": 10,
        "clipsize": 16,
        "reload": 15,
        "range": 300,
        "weight": 15,
        "power": 15,
        "cost": 43000,
        "currency": "silver",
        "info": "Basically a bunch of huge shotguns capable of damaging multiple targets with explosions and a shower of rockets. Deadly at really close distances. Long reload time. Great finisher.",
        "durability": 0
    },
    {
        "id": 7,
        "name": "CRV Pin",
        "image": weaponImagePath + "pin.png",
        "slot": "standard",
        "ammotype": "rocket",
        "aimingtype": "direct",
        "maxlevel": 8,
        "level": 0,
        "playerlevel": 4,
        "damage": 1318,
        "firerate": 2.5,
        "clipsize": 4,
        "reload": 12,
        "range": 500,
        "weight": 18,
        "power": 10,
        "cost": 43000,
        "currency": "silver",
        "info": "Big blasting radius and long range makes this weapon pretty handy at middle distances. Long reload time.",
        "durability": 0
    },
    {
        "id": 9,
        "name": "SM NORICUM",
        "image": weaponImagePath + "noricum.png",
        "slot": "standard",
        "ammotype": "mortar",
        "aimingtype": "direct",
        "maxlevel": 8,
        "level": 0,
        "playerlevel": 5,
        "damage": 239,
        "firerate": 20,
        "clipsize": 14,
        "reload": 7,
        "range": 800,
        "weight": 20,
        "power": 0,
        "cost": 360000,
        "currency": "silver",
        "info": "High-angle multiple mortars that flush the enemy out of the cover. Very usable for wearing down opponents at a distance over the length of a game. Short reload time.",
        "durability": 0
    },
    {
        "id": 12,
        "name": "ECC Thunder",
        //"image": weaponImagePath + "ecu.png",
        "slot": "heavy",
        "ammotype": "",
        "aimingtype": "direct",
        "maxlevel": 8,
        "level": 0,
        "playerlevel": 3,
        "damage": 471,
        "firerate": 0,
        "clipsize": 5,
        "reload": 0,
        "range": 500,
        "weight": 20,
        "power": 50,
        "cost": 500,
        "currency": "gold"
    },
    {
        "id": 10,
        "name": "KwK 448mm",
        "image": weaponImagePath + "kwk.png",
        "slot": "heavy",
        "ammotype": "cannon",
        "aimingtype": "direct",
        "maxlevel": 8,
        "level": 0,
        "playerlevel": 3,
        "damage": 5324,
        "firerate": 1,
        "clipsize": 1,
        "reload": 10,
        "range": 1500,
        "weight": 40,
        "power": 0,
        "cost": 20000,
        "currency": "silver",
        "info": "Heavy long range weapon. Deals huge one-time damage. Note that a upgraded KwK packs some serious damage with a shorter reload-time then the Kang-dae.",
        "durability": 0
    },
    {
        "id": 11,
        "name": "ETC Kang-Dae",
        "image": weaponImagePath + "kangdae.png",
        "slot": "heavy",
        "ammotype": "cannon",
        "aimingtype": "direct",
        "maxlevel": 8,
        "level": 0,
        "playerlevel": 5,
        "damage": 8785,
        "firerate": 1,
        "clipsize": 1,
        "reload": 15,
        "range": 1500,
        "weight": 40,
        "power": 0,
        "cost": 360000,
        "currency": "silver",
        "info": "An advanced electrothermal-chemical gun. More damage and missile-velocity compared to the KwK, at the expense of a long reload time. Most likely the reason you got killed in your last game.",
        "durability": 0
    }

];

// Model: Weapons with calculated "DPS"
var weaponsDPS = function () {
    var data = weapon.list();
    data.forEach(function (item) {
        if (!item.durability) {
            var damagePerClip = item.damage * item.firerate * item.clipsize;
            var clipEmpy = Math.round((item.clipsize / item.firerate) * 100) / 100; // This one might be wrong, see EE Aphid
            var ClipTime = clipEmpy + item.reload;
            var ClipsPerMatch = Math.floor(600 / ClipTime);
            var damagePerMatch = ClipsPerMatch * damagePerClip;
            item.clipEmpy = clipEmpy;
            item.damagePerClip = damagePerClip;
            item.clipsPerMatch = ClipsPerMatch;
            item.damagePerMatch = damagePerMatch;
        } else {
            // This is a shield
            item.clipEmpy = "-";
            item.damagePerClip = "-";
            item.clipsPerMatch = "-";
            item.damagePerMatch = "-";
        }
    });
    return data;
};

//
//  Model: Weaponupgrades
//  NOTE: This array must be sorted correctly!
//
/*
var weaponUpgrades = [

    {
        // GAU Punisher
        "weaponid": 0,  // Must correnspond with id in weapons
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[0].damage // Change this, be careful with ID's
            },
            {
                "level": 2,
                "cost": 2000, // Currency is in weapons-model
                "repairTime": 1800, // In seconds, total time
                "upgradeTime": 2400, // In seconds
                "damage": 134 // Total sum, not the difference
            }
            ,
            {
                "level": 3,
                "cost": 3000,
                "repairTime": 2400,
                "upgradeTime": 3600,
                "damage": 150
            }
            ,
            {
                "level": 4,
                "cost": 4000,
                "repairTime": 4400,
                "upgradeTime": 4600,
                "damage": 450
            }
            ,
            {
                "level": 5,
                "cost": 5000,
                "repairTime": 5400,
                "upgradeTime": 5600,
                "damage": 550
            }
        ]
    }
    ,
    {
        // AC Malot
        "weaponid": 1,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[1].damage
            },
            {
                "level": 2,
                "cost": 20001,
                "repairTime": 18001,
                "upgradeTime": 24001,
                "damage": 1341
            },
            {
                "level": 3,
                "cost": 30001,
                "repairTime": 24001,
                "upgradeTime": 36001,
                "damage": 1501
            }
        ]
    }
    ,
    {
        // AT Spiral
        "weaponid": 2,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[2].damage
            }
        ]
    }
    ,
    {
        // AT pinata
        "weaponid": 3,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[3].damage
            }
        ]
    }
    ,
    {
        // CRV PIN
        "weaponid": 4,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[4].damage
            }
        ]
    }
    ,
    {
        // APHID
        "weaponid": 5,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[5].damage
            }
        ]
    }
    ,
    {
        // SM NORICUM
        "weaponid": 6,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[6].damage
            }
        ]
    }
    ,
    {
        // KWK
        "weaponid": 7,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[7].damage
            }
        ]
    }
    ,
    {
        // ETC KANG DAE
        "weaponid": 8,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[8].damage
            }
        ]
    }
    ,
    {
        // ECU
        "weaponid": 9,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": 0
            }
        ]
    }
    ,
    {
        // MALOT T
        "weaponid": 10,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[10].damage
            }
        ]
    }
    ,
    {
        // PUNISHER T
        "weaponid": 11,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[1].damage
            }
        ]
    }
    ,
    {
        // THUNDER
        "weaponid": 13,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "damage": weapons[12].damage
            },
            {
                "level": 2,
                "cost": 200,
                "repairTime": 2000,
                "upgradeTime": 2000,
                "damage": 560
            }
        ]
    }


];

*/

//
//  Model: Robot Upgrades
//
var robotUpgrades = [
    {
        "robotid": 0,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "power": robots[0].power,
                "weight": robots[0].weight,
                "durability": robots[0].durability,
                "speed": robots[0].speed
            },
            {
                "level": 2,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "power": 462,
                "weight": 47,
                "durability": 31200,
                "speed": 73
            }
        ]
    },
    {
        "robotid": 1,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "power": robots[1].power,
                "weight": robots[1].weight,
                "durability": robots[1].durability,
                "speed": robots[1].speed
            }
        ]
    },
    {
        "robotid": 2,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "power": robots[2].power,
                "weight": robots[2].weight,
                "durability": robots[2].durability,
                "speed": robots[2].speed
            }
        ]
    },
    {
        "robotid": 3,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "power": robots[3].power,
                "weight": robots[3].weight,
                "durability": robots[3].durability,
                "speed": robots[3].speed
            }
        ]
    },
    {
        "robotid": 4,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "power": robots[4].power,
                "weight": robots[4].weight,
                "durability": robots[4].durability,
                "speed": robots[4].speed
            }
        ]
    },
    {
        "robotid": 5,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "power": robots[5].power,
                "weight": robots[5].weight,
                "durability": robots[5].durability,
                "speed": robots[5].speed
            }
        ]
    },
    {
        "robotid": 6,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "power": robots[6].power,
                "weight": robots[6].weight,
                "durability": robots[6].durability,
                "speed": robots[6].speed
            }
        ]
    },
    {
        "robotid": 7,
        "data": [
            {
                "level": 1,
                "cost": 0,
                "repairTime": 0,
                "upgradeTime": 0,
                "power": robots[7].power,
                "weight": robots[7].weight,
                "durability": robots[7].durability,
                "speed": robots[7].speed
            }
        ]
    }

];




//
// Model: Releases
//
var releases = [
    {
        "id": 0,
        "version": "0.1",
        "releaseDate": "2014-04-14",
        "url": "",
        "releaseNotes": "First release!"
    },

    {
        "id": 1,
        "version": "0.1.1",
        "releaseDate": "2014-05-04",
        "url": "http://wwr.mobi/forum/russian-sector/pixonic/updates/1865-update-0-1-1",
        "releaseNotes": "New spawnpoints. Max players are 6 in each team. More fair and stable matchmaking."
    },

    {
        "id": 2,
        "version": "0.1.2",
        "releaseDate": "2014-05-12",
        "url": "http://wwr.mobi/forum/russian-sector/pixonic/updates/3371-update-0-1-2",
        "releaseNotes": "Target capturing reverted. Minor bug fixes."
    },

    {
        "id": 3,
        "version": "0.2.0",
        "releaseDate": "2014-06-02",
        "url": "http://wwr.mobi/forum/russian-sector/pixonic/updates/4309-update-0-2-0",
        "releaseNotes": "Game Center cloud profile. New robot Cossack introduced with a standard hardpoint.",
        "robots": [{id: 2, "name": "Cossack"}]
    },

    {
        "id": 4,
        "version": "0.2.1",
        "releaseDate": "2014-06-26",
        "url": "http://wwr.mobi/forum/russian-sector/pixonic/updates/7033-update-0-2-1",
        "releaseNotes": "Medium hardpoint introduced. New robot: Boa. Changes to Cossack. New Weapons: EE Aphid, GAU Punisher T, AC Malot T. Floors added to buildings to prevent Natacha sniping.",
        "robots": [{id: 5, "name": "Boa"}],
        "weapons": [{id: 5, "name": "EE Aphid"}, {id: 11, "name": "Gau Punisher T"}, {id: 10, "name": "AC Malot T"}]
    },

    {
        "id": 5,
        "version": "0.3.0",
        "releaseDate": "2014-07-18",
        "url": "http://wwr.mobi/forum/russian-sector/pixonic/updates/8928-update-0-3-0",
        "releaseNotes": "Teams Added. New Weapons: EEC Thunder. Changes to Game Center connectivity. Changes in speed to all robots.",
        "weapons": [{id: 13, "name": "ECC Thunder"}]

    }
];


//
//  Model: News
//
var news = [
    {
        "id": 0,
        "date": "2014-06-14",
        "subject": "First release for this Guide App!",
        "extract": "First release!",
        "notes": "First release!"
    },
    {
        "id": 1,
        "date": "2014-06-18",
        "subject": "Version 0.3.0 Brings New Weapon",
        "extract": "The ECC Thunder are here!",
        "notes": "The ECC Thunder are here! Short-range weapon for the heavy slot"
    },
    {
        "id": 2,
        "date": "2014-06-21",
        "subject": "New maps are coming",
        "extract": "Two new maps will be featured in the next release.",
        "notes": "Two new maps will be featured in the next release."
    }
];

