//
controller = function (hash) {
    if (hash === "") {
        hash = "#start";
        location.hash = hash;
    }

    else if (hash === "#start") {
        panes.render("pane-start", "WWR Guide");
    }

    else if (hash === "#error") {
        panes.view("#error-view", 0, "errorPlaceholder");
        panes.render("pane-error", "Error");
    }
    else if (hash === "#listrobots") {
        panes.render("pane-robots", getMenuLabel("listrobots"));
    }

    else if (hash === "#listweapons") {
        panes.render("pane-weapons", getMenuLabel("listweapons"));
    }

    else if (hash === "#compareweapons") {
        panes.view("#compareweapons-view", weaponsDPS(), "compare-weapons");
        panes.render("pane-compareweapons", getMenuLabel("compareweapons"));
        $("#compare-weapons-table").tablesorter();
        $('.number').number(true);
    }

    else if (hash === "#maps") {
        panes.render("pane-maps", getMenuLabel("maps"));
    }

    else if (hash === "#releases") {
        panes.render("pane-releases", getMenuLabel("releases"));
    }
    else if (hash === "#links") {
        panes.render("pane-links", getMenuLabel("links"));
    }

    else if (hash === "#about") {
        panes.render("pane-about", getMenuLabel("about"));
    }

    else if (hash.substring(0, 7) === "#robot-") {
        var id = hash.substr(hash.lastIndexOf("-") + 1);
        renderRobot(id);
    }

    else if (hash.substring(0, 8) === "#weapon-") {
        var id = hash.substr(hash.lastIndexOf("-") + 1);
        renderWeapon(id);
    }

    else if (hash === "#news") {
        renderNews();
    }

    else if (hash.substring(0, 10) === "#newsitem-") {
        var id = hash.substr(hash.lastIndexOf("-") + 1);
        renderNewsItem(id);
    }

    else {
        hash = "#start";
        location.hash = hash;
    }

};


//
//
//  Class: Robot
//
//  Will create robot[id] as dynamic global object at init()
//
////////////////////////////////////////////////////////////

var robot = new function () {
    "use strict";

    // Public
    this.index = [];

    this.selected = function () {
        if ($("#selectedRobot").val()) {
            return $("#selectedRobot").val();
        } else {
            return false;
        }
    };

    this.size = function () {
        return robots.length;
    };

    this.list = function () {
        var data = [];
        this.index.forEach(function (id) {
            data.push(robot[id]);
        });
        return data;
    };

    this.get = function (id) {
        if (robot[id]) {
            return robot[id];
        } else {
            return false;
        }
    };

    this.level = function (id) {
        return level(id);
    };

    this.save = function(id,robotLevel) {
        writeLocalStorage("robot." + id + ".level", robotLevel);
        var data = calculate(robot[id]);
        robot[id] = data;
    };

    this.selectWeapon = function (id, slot) {
        closeWeaponSelector();
        var selectedRobot = this.selected();
        var slotIndex = slot.replace("HP", "") - 1;
        var savedWeapons = [];

        if (checkLocalStorage("robot." + selectedRobot)) {
            savedWeapons = readLocalStorage("robot." + selectedRobot);
        } else {
            var hardpoints = robot[selectedRobot].hardpoints.length;
            for (var i = 0; i < hardpoints; i++) {
                savedWeapons.push("");
            }
        }
        savedWeapons.forEach(function (value, key) {
            if (key === slotIndex) {
                savedWeapons[key] = id;
            }
        });
        writeLocalStorage("robot." + selectedRobot, savedWeapons);
        var data = calculate(robot[selectedRobot]);
        robot[selectedRobot] = data;
        renderRobot(selectedRobot);
    };

    // INITIALIZE
    this.init = function () {
        var index = [];
        robots.forEach(function (item) {
            calculate(item);
            index.push(item.id);
            robot[item.id] = item;
        });
        // Update index with id's
        this.index = index;
    };


    // Private
    function level(id) {
        if (checkLocalStorage("robot." + id + ".level")) {
            return readLocalStorage("robot." + id + ".level");
        } else {
            return 1; // Level 1
        }
    }

    function upgradeInfo(id, robotLevel) {
        var index = getIndexOf(robotUpgrades, "robotid", id);
        if (robotUpgrades[index]) {
            var data = robotUpgrades[index].data;
            data.forEach(function (item) {
                if (item.level === robotLevel) {
                    item.active = true;
                } else {
                    item.active = false;
                }
            });
            return(data);
        } else {
            return false;
        }
    }

    function levelInfo(id, robotLevel) {
        var index = getIndexOf(robotUpgrades, "robotid", id);
        var data = robotUpgrades[index].data;
        var levelIndex = getIndexOf(data, "level", robotLevel);
        return robotUpgrades[index].data[levelIndex];
    }

    function calculate(item) {
        // Update with correct level data
        var robotLevel = level(item.id);
        var levelData = levelInfo(item.id, robotLevel);
        item.level = robotLevel;
        item.upgradeInfo = upgradeInfo(item.id, robotLevel);
        item.speed = levelData.speed;
        item.durability = levelData.durability;
        item.power = levelData.power;
        item.weight = levelData.weight;
        // Read saved weapons from localStorage
        // Add weapon info for each hardpoint to robot[x] as item
        var savedWeapons = readLocalStorage("robot." + item.id);
        item.hardpoints.forEach(function (value, key) {
            if (savedWeapons[key]) {
                item.hardpoints[key].weapon = weapon[savedWeapons[key]];
            } else {
                item.hardpoints[key].weapon = false;

            }
        });
        // Calculate the robots weight including weapons
        var totalWeight = 0;
        var totalPower = 0;
        item.hardpoints.forEach(function (value, key) {
            if (item.hardpoints[key].weapon) {
                totalWeight = totalWeight + item.hardpoints[key].weapon.weight;
                totalPower = totalPower + item.hardpoints[key].weapon.power;
            }
        });
        item.totalWeight = totalWeight;
        item.totalPower = totalPower;
        item.weightWarning = false;
        item.powerWarning = false;
        if (totalWeight > item.weight) {
            item.weightWarning = true;
        }
        if (totalPower > item.power) {
            item.powerWarning = true;
        }


        return item;
    }
};


//
//
//  Class: Weapons
//
//  Will create weapon[id] as dynamic global object at init()
//
///////////////////////////////////////////////////////////////

var weapon = new function () {

    "use strict";

    // Public
    this.index = [];

    this.size = function () {
        return weapons.length;
    };

    this.selected = function () {
        if ($("#selectedWeapon").val()) {
            return $("#selectedWeapon").val();
        } else {
            return false
        }
    };

    this.get = function (id) {
        if (weapon[id]) {
            return weapon[id];
        } else {
            return false;
        }
    };

    this.list = function () {
        var data = [];
        this.index.forEach(function (id) {
            data.push(weapon[id]);
        });
        return data;
    };


    this.bySlot = function (type) {
        //TODO refactor to use list or index
        var data = [];
        weapons.forEach(function (entry) {
            if (entry.slot === type) {
                data.push(entry);
            }
        });
        return data;
    };

    this.save = function (id, weaponLevel) {
        writeLocalStorage("weapon." + id + ".level", weaponLevel);
        var data = calculate(weapon[id]);
        weapon[id] = data;
    };

    this.level = function (id) {
        return level(id);
    };

    // INITIALIZE
    this.init = function () {
        var index = [];
        weapons.forEach(function (item) {
            calculate(item);
            index.push(item.id);
            weapon[item.id] = item;
        });
        // Update index with id's
        this.index = index;
    };

    // Private
    function level(id) {
        if (checkLocalStorage("weapon." + id + ".level")) {
            return readLocalStorage("weapon." + id + ".level");
        } else {
            return 1; // Level 1
        }
    }

    function upgradeInfo(id, weaponLevel) {
        var index = getIndexOf(weaponUpgrades2, "weaponid", id);
        if (weaponUpgrades2[index]) {
            var data = weaponUpgrades2[index].data;
            data.forEach(function (item) {
                if (item.level === weaponLevel) {
                    item.active = true;
                } else {
                    item.active = false;
                }
            });

            data.weaponname = weaponUpgrades2[index].weaponname;
            return(data);
        } else {
            return false;
        }
    }

    function levelInfo(id, weaponLevel) {
        var index = getIndexOf(weaponUpgrades2, "weaponid", id);
        var data = weaponUpgrades2[index].data;
        var levelIndex = getIndexOf(data, "level", weaponLevel);
        return weaponUpgrades2[index].data[levelIndex];
    }

    function calculate(item) {
        // This function is used in init() BEFORE global objects are created
        // and must be used AFTER save to localStorage
        var weaponLevel = level(item.id);
        var levelData = levelInfo(item.id, weaponLevel);
        item.level = weaponLevel;
        item.upgradeInfo = upgradeInfo(item.id, weaponLevel);
        item.damage = levelData.damage;
        return item;
    }

};


//
//
//  Class: Panes
//
///////////////////////////////////////////////////////////////

var panes = new function (controller) {



    this.controller = function() {
        return controller;
    };


    // INITIALIZE
    this.init = function() {
        $(window).bind('hashchange', function (e) {
            var previousURL = e.originalEvent.oldURL;
            var previousHash = previousURL.substr(previousURL.lastIndexOf("#"));
            controll(location.hash, previousHash);
        });
        controll(location.hash);
    };

    this.set = function (hash) {
        var path = window.location.pathname;
        window.location.href = path + '#' + hash;
    };

    this.view = function (view, data, placeHolder) {
        var item = $(view).mcomponent({
            model: data,
            placeHolderId: placeHolder
        });
        item.render();
        // Call number() after render
        $('.number').number(true);
    };

    this.render = function (pane, title) {
        $(".pane").hide();
        window.document.title = title;
        $("#" + pane).show();
        $("#wrapper").scrollTop(0);
    };

    function save(hash, previousHash) {
        writeLocalStorage("hash", hash);
        if (previousHash) {
            writeLocalStorage("previousHash", previousHash);
        } else {
            removeLocalStorage("previousHash");
        }
    }

    var controll = function (hash, previousHash) {
        save(hash, previousHash);
        this.controller(hash);
    };
};


//
//  RENDER
//

function renderInitialViews() {
    panes.view("#menu-view", menu, "menu");
    panes.view("#menu-short-view", getMenuShort(), "menuShort");
    panes.view("#news-view", getNews(3), "cirkus-news");
    panes.view("#robot-list-view", robot.list(), "robot-list");
    panes.view("#weapon-list-view", weapon.list(), "weapon-list");
    panes.view("#compareweapons-view", weaponsDPS(), "compare-weapons");
    panes.view("#releases-view", releases, "releases");
}

function showError(id) {
    panes.view("#error-view", id, "errorPlaceholder");
    $("#pane-error").show();
}

function renderWeaponSelector(target, data, slot) {
    var weaponSelectorView = $("#weaponselector-view").mcomponent({
        model: data,
        placeHolderId: target
    });
    weaponSelectorView.render();
}

function renderWeapon(id) {
    if (weapon[id]) {
        panes.view("#weapon-view", weapon[id], "weapon");
        panes.render("pane-weapon", weapon[id].name);
        $("#selectedWeapon").val(id);
        translateView("#weapon");
    } else {
        showError(3);
        panes.render("pane-error", "Error");
    }
}

function renderRobot(id) {
    if (robot[id]) {
        panes.view("#robot-view", robot[id], "robot")
        panes.render("pane-robot", robot[id].name);
        $("#selectedRobot").val(id);
        translateView("#robot");
    } else {
        showError(3);
        panes.render("pane-error", "Error");
    }
}


//
//  Menu
//

function getMenuLabel(url) {
    var index = getIndexOf(menu, "url", url)
    var data = menu[index];
    if (data) {
        return(data.label);
    } else {
        return false;
    }
}

function getMenuShort() {
    var data = [];
    menuShort.forEach(function(value, key){
        data.push(menu[key]);
    });
    return data;
}

//
//  News
//
function renderNewsItem(id) {
    var index = getIndexOf(news, "id", id);
    var data = news[index];
    if (data) {
        title = "News";
        panes.view("#news-item-view", data, "news-item");
        panes.render("pane-news", title);
        translateView("#news-view");
    }
}

function renderNews() {
    var data = getNews(10);
    panes.view("#news-view", data, "news-list");
    panes.render("pane-newslist", "News");
    translateView("#news-view");
}

function getNews(items) {
    if (!items) {
        items = 3;
    }
    var data = news;
    data.reverse();
    var dataSelection = data.slice(0, items);
    return(dataSelection);
}

//
//  Weapon selector
//


// Called from onClick in view
function showWeaponSelector(name, type, that) {
    var data = weapon.bySlot(type);
    // Special treatment for "Medium"
    if (type === "medium") {
        data = data.concat(weapon.bySlot("standard"));
    }
    data.targetSlot = name;
    var buttonTop = that[0].offsetTop;
    var buttonLeft = that[0].offsetLeft;
    var winHeight = $("body").height();
    //todo rename winheight to bodyHeight
    //todo measure the real win height, and see if its larger then the body height (=really large screens)
    var target = "weapon-selector";
    panes.view("#weaponselector-view", data, target);
    translateView("#weaponselector-list");
    $("#" + target).css("display", "block")
        .css("top", buttonTop + "px")
        .css("left", buttonLeft + "px");
    $("#weapon-selector-overlay")
        .css("display", "block")
        .css("height", winHeight + "px");
}

function closeWeaponSelector() {
    $("#weapon-selector").css("display", "none");
    $("#weapon-selector-overlay").css("display", "none");
}

//  Weapons comparison
function filterWeaponsComparison(type) {
    if (type === undefined) {
        $("#compare-weapons-table tr.row").show();
    } else {
        $(this).addClass("active");
        $("#compare-weapons-table tr.row").hide();
        $("#compare-weapons-table tr.ammotype-" + type).show();
    }
}

//
// Weapon Upgrades
//

// Called from onClick in view
//TODO move this into weapon()
function clickWeaponUpgradeLevel(level) {
    var id = weapon.selected();
    weapon.save(id, level)
    renderWeapon(id);
}


function clickrobotUpgradeLevel(level) {
    var id = robot.selected();
    robot.save(id, level)
    renderRobot(id);
}

//
//  TRANSLATIONS
//

function setLanguage(lang) {
    writeLocalStorage("lang", lang);
    translator = $('body').translate({lang: lang, t: dict});
    setLanguageIndicator();
}

function translateView(view) {
    if (checkLocalStorage("lang")) {
        var language = readLocalStorage("lang");
        translator = $(view).translate({lang: language, t: dict});
    }
}

function setLanguageIndicator() {
    if ("translator" in window) {
        var lang = translator.lang();
        $(".langselector").each(function () {
            $(this).removeClass("active");
            if (($(this).attr("value")) == lang) {
                $(this).addClass("active");
            }
        });
    } else {
        $(".langselector-en").addClass("active");
    }
}


//
// Initialize
//
window.onload = function () {
    //
    // Do not proceed unless localstorage are enabled.
    //
    if (!supportsLocalStorage()) {
        window.localStorageEnabled = false;
        showError(1);
        return false;
    } else if (!privateModeEnabled()) {
        window.localStorageEnabled = false;
        showError(2);
        return false;
    } else {
        window.localStorageEnabled = true;
        //
        //  Start app
        //

        weapon.init();
        robot.init();
        panes.init();
        renderInitialViews();
        $(".static-gui").show();
        translateView("body");
        setLanguageIndicator();
        $("#compare-weapons-table").tablesorter();
        // Listeners
        $(".filter .item").click(function () {
            $(".filter .item").removeClass("active");
            $(this).toggleClass("active", true);
        });
    }
};


function testData(id) {
    log(weapon[id].upgradeInfo.weaponname);
    log(weapon[id].name);
}