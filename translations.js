
var dict = {
    //
    //  Menu, page titles
    //
    "Start": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Robots": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Weapons": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Compare Weapons": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Maps": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Links": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "About": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Releases": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },

    //
    //   Measurements, units
    //
    "second": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "seconds": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "meter": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "meters": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "gold": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "silver": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Ton": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "ton": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "tons": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Mw": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "km/h": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },


    //
    //  GUI words
    //
    "Name": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Durability": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Speed": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Cost": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Power": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Weight": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Class": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Max level": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Available at level": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Slot": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Ammotype": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Aiming": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Range": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Damage": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Firerate": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Clipsize": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Reload time": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Reload": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Close": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Clip Empty": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "DMG/Clip": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Clips/Match": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "DMG/Match": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },


    //
    //  Types
    //
    "light": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "standard": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "medium": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "heavy": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "bullet": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "rocket": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "mortar": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "cannon": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Bullet": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Rocket": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Mortar": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Cannon": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "direct": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "homing": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },

    //
    //  Filter
    //
    "Filter": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Show All": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },

    //
    //  Strings
    //
    "WWR Guide": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Guide for Walking War Robots": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "Robots, Weaponstats, Maps and useful links for Pixonic game Walking War Robots": {
        de: "GERMAN",
        ru: "RUSSIAN"
    },
    "This app is not affiliated in any way with Pixonic.": {
        de: "GERMAN",
        ru: "RUSSIAN"
    }


};






