WWR GUIDE
Robots, Weaponstats, Maps and useful links for Pixonic game Walking War Robots.
This app is not affiliated in any way with Pixonic.

Latest master can be found here: http://wwrguide.bitbucket.org/


//TODO

Dataimport:

install rockmongo on localhost
use mongoimport to setup that excel as a db
setup a node.js project with separate git
use that node server to build the file data.js



View-item for releases, id on release number
Add "introduced" for model with version number info when new items are added, linking to releases
Add "Help" accessible in About and as a button in gui
Add model and style for links
Add handling for responsive tables to Compare Weapons

Test in Firefox, Safari, IE for desktop
Construct a responsive button-menu for mobile

Check that all strings are translated
Translate About/Contact pane?

Find a plugin for maps:
http://codecondo.com/jquery-plugins-manipulating-world-maps/
http://jhere.net/docs.html
http://jquerygeo.com/
http://wayfarerweb.com/jquery/plugins/mapbox/

//MAYBE
back-button based on previousHash (getMenuLabel) ??
Refactor for node.js ?

//FIXED
Add levels to robot[]
Rebuild Robot.calculate. Varning when robot are to heavily loaded/power limit are reached.
Add links to footer, new array menuShort containing references to menu[]
Unified model holding all robot + weapons
Refactor to classes for weapon and robot
getWeaponInfo are used alot, maybe save the objects instead?
filesizes, compress files
Remodel for responsive down to mobilephone (320px)
Add "Releases" as pane with version info for linking.
Add model and functions to handle upgrade-levels in localStorage (data missing).
show error(3) when id not found for weapons
Rewrite handling of localStorage to reduce checks
Check that localStorage-functions don't b0rk on private surfing mode
Crunched PNGs
Adjusted tiling background
Faster initial rendering
Add handling for number formatting with locale
Add "about" page
Filter for weapontypes in Compare Weapons
Add images for weapons from screenshots
Medium hardpoints can fit standard weapons as well
Sorting for columns in Compare Weapons
localStorage for all robot-id's
Add handling of cost on Silver vs Gold to model: currency (gold/silver)
Sort CSS error for spans
Remove font awesome
Merge all js-libs into libs.js
Translate


